"""Servidor."""
import socketserver
import sys
import simplertp
import secrets
from xml.sax import make_parser
from xml.sax.handler import ContentHandler
import time
import random

timex = time.time()
timey = time.mktime((time.gmtime()[0], time.gmtime()[1], time.gmtime()[2],
                     0, 0, 0, 0, 0, 0))
tiempox = int(timex - timey)
tiempoy = time.strftime('%Y%m%d', time.gmtime(timex))

tiempo = str(tiempoy) + " " + str(tiempox)


class SmallSMILHandlers(ContentHandler):
    """Clase SmallSMILHandlers."""

    def _init_(self):
        """Lista de datos."""
        self.LISTA = []
        self.DICCIONARIO = {
            "account": ["username", "passwd"],
            "uaserver": ["ip", "puerto"],
            "rtpaudio": ["puerto"],
            "regproxy": ["ip", "puerto"],
            "log": ["path"],
            "audio": ["path"]}

    def startElement(self, elemento, attrs):
        """Elemento."""
        if elemento in self.DICCIONARIO:
            LISTAS = {}
            for j in self.DICCIONARIO[elemento]:
                LISTAS[j] = attrs.get(j, "")
            self.LISTA.append([elemento, LISTAS])

    def get_tags(self):
        """get_tags."""
        return self.LISTA


def log(type, servidorr, puertor, message):
    """log."""
    fichero = open(log, "a")
    text = message.replace("\r\n", " ")
    if type:
        recieved = "Recieved from:"
    else:
        recieved = "Sent to:"
    fichero.write(tiempo + " " + recieved + servidorr + ":" + str(puertor)
                  + ":" + text + "\n")
    fichero.close()


class EchoHandler(socketserver.DatagramRequestHandler):
    """Echo server class."""

    def handle(self):
        """Echo server class."""
        # Escribe dirección y puerto del cliente (de tupla client_address)
        datos = self.rfile.read()
        receipt = datos.decode("utf-8")

        metodo = receipt.split(" ")[0]
        metodos = receipt.split(" ")[0]

        if metodo == "REGISTER":
            received = tiempo + " Recived from: " + proxydireccion
            received += ":" + str(proxypuerto) + ": " + receipt
            log(True, self.direccioncliente[0],
                self.direccioncliente[1], str(received))

        if metodo == "INVITE":
            received = tiempo + " Recived from: " + proxydireccion
            received += ":" + str(proxypuerto) + ": " + receipt
            log(True, self.direccioncliente[0],
                self.direccioncliente[1], str(received))

            write = "SIP/2.0 100 Trying SIP/2.0 180 Ring  " + \
                    "SIP/2.0 200 OK\r\n"

            datos = "v=0" + "\r\n"
            datos += "o=" + nombreusuario + " " + servidor + "\r\n"
            datos += "s=misesion" + "\r\n"
            datos += "t=0" + "\r\n"
            datos += "m=audio " + str(rtpuerto) + " RTP"
            writes = "Content-Type: application/sdp" + "\r\n"
            writes += "Content-Length: " + str(
                        len(datos)) + "\r\n\r\n"
            ship = write + writes + datos
            self.wfile.write(str.encode(ship))

        if metodos == "ACK":
            received = tiempo + " Recived from " + proxydireccion
            received += ":" + str(proxypuerto) + ": " + receipt

            log(True, self.direccioncliente[0],
                self.direccioncliente[1], str(receipt))
            aleatorio = random.randint(0, 1000)
            marker = secrets.randbits(1)
            cabeceraRTP = simplertp.RtpHeader()
            numer = aleatorio.randint(0, 15)
            csrcs = [aleatorio.randint(0, 2**10)for _ in range(numer)]
            cabeceraRTP.set_header(version=2, marker=marker, payload_type=14,
                                   ssrc=aleatorio, pad_flag=0, cc=len(csrcs))
            cabeceraRTP.setCSRC(csrcs)
            RTP_audio = simplertp.RtpPayloadMp3(audio)
            simplertp.send_rtp_packet(cabeceraRTP, RTP_audio,
                                      servidor, int(rtpuerto))

        if metodo == "BYE":
            received = tiempo + " Recived from " + proxydireccion
            received += ":" + str(proxypuerto) + ": " + receipt
            log(True, self.direccioncliente[0],
                self.direccioncliente[1], str(received))

        if metodo not in ("INVITE", "ACK", "BYE", "REGISTER"):
            received = tiempo + " Recived from " + proxydireccion
            received += ":" + str(proxypuerto) + ": " + receipt
            log(True, self.direccioncliente[0],
                self.direccioncliente[1], str(received))
            writes405 = b"SIP/2.0 405 Method Not Allowed\r\n"
            self.wfile.write(writes405)
            log(False, self.direccioncliente[0],
                self.direccioncliente[1], str(writes405))

        else:
            received = tiempo + "Recived from " + servidor
            received += ":" + str(puerto) + ": " + receipt
            log(True, self.direccioncliente[0],
                self.direccioncliente[1], str(received))
            writes400 = b"SIP/2.0 400 Bad Request\r\n"
            self.wfile.write(writes400)
            log(False, self.direccioncliente[0],
                self.direccioncliente[1], str(writes400))


if __name__ == "_main_":

    try:
        configuracion = sys.argv[1]

    except IndexError:
        sys.exit("Usage: python3 server.py config ")

    parser = make_parser()
    cHandler = SmallSMILHandlers()
    parser.setContentHandler(cHandler)
    parser.parse(open(configuracion))

    xml = cHandler.get_tags()
    nombreusuario = xml[0][1]["username"]
    contraseña = xml[0][1]["passwd"]
    servidor = xml[1][1]["ip"]
    puerto = xml[1][1]["puerto"]
    rtpuerto = xml[2][1]["puerto"]

    proxydireccion = xml[3][1]["ip"]
    proxypuerto = xml[3][1]["puerto"]
    log = xml[4][1]["path"]
    audio = xml[5][1]["path"]

    server = socketserver.UDPServer((servidor, int(puerto)), EchoHandler)
    listening = "Listening"
    log(True, servidor, puerto, str(listening))
    server.serve_forever()
