"""Registrar proxy."""
import socket
import socketserver
import sys
from xml.sax import make_parser
from xml.sax.handler import ContentHandler
import time
import json

timex = time.time()
timey = time.mktime((time.gmtime()[0], time.gmtime()[1], time.gmtime()[2],
                     0, 0, 0, 0, 0, 0))
tiempox = int(timex - timey)
tiempoy = time.strftime('%Y%m%d', time.gmtime(timex))

tiempoz = str(tiempoy) + " " + str(tiempox)


class SmallSMILHandlerp(ContentHandler):
    """SmallSMILHandlerp."""

    def _init_(self):
        """Lista de datos."""
        self.LISTA = []

        self.DICCIONARIO = {
            "server": ["name", "ip", "puerto"],
            "database": ["path", "passwdpath"],
            "log": ["path"]}

    def startElement(self, elemento, attrs):
        """startElement."""
        if elemento in self.DICCIONARIO:
            LISTAS = {}
            for j in self.dicc[elemento]:
                LISTAS[j] = attrs.get(j, "")
            self.LISTA.append([elemento, LISTAS])

    def get_tags(self):
        """get_tags."""
        return self.LISTA


def log(type, servidorr, puertor, message):
    """log."""
    fichero = open(log, "a")
    text = message.replace("\r\n", " ")
    if type:
        recieved = "Recieved from:"
    else:
        recieved = "Sent to:"
    fichero.write(tiempoz + " " + recieved + servidorr + ":" + str(puertor)
                  + ":" + text + "\n")
    fichero.close()


class ProxyEchoHandler(socketserver.DatagramRequestHandler):
    """Clase ProxyEchoHandler."""

    registro = {}

    def registrojson(self):
        """registrojson."""
        with open(log, 'w') as ficherojson:
            json.dump(self.registro, ficherojson, indent=3)

    """Echo server class."""
    def handle(self):
        """manejador."""
        servidor = self.direccioncliente[0]
        puerto = self.direccioncliente[1]
        dato = self.rfile.read()
        receipt = dato.decode("utf-8")

        receipt1 = (receipt + "\r\nVia: SIP/2.0/UDP "
                    + servidor + ":" + puerto + "\r\n ")
        received = tiempoz + " Recived from: " + servidor
        received += ":" + str(puerto) + ": " + receipt
        log(True, self.direccioncliente[0],
            self.direccioncliente[1], str(received))
        nombreusuario = receipt.split(" ")[1]
        metodo = receipt.split(" ")[0]
        if metodo == "REGISTER":
            expirar = receipt.split(" ")[3]
            tiempo = time.time()
            times = tiempo + int(expirar)
            tiempoexpiracion = time.strftime('%Y%m%d%H%M%S',
                                             time.gmtime(times))
            tiempoactividad = time.strftime('%Y%m%d%H%M%S',
                                            time.gmtime(time.time()))

            self.registro[nombreusuario] = [servidor,
                                            int(puerto), int(tiempo), expirar]

            self.register2json()

            if tiempoexpiracion <= tiempoactividad:

                del self.registro[nombreusuario]
                self.registrojson()

            answer = b"SIP/2.0 200 OK\r\n"
            self.wfile.write(answer)

            log(False, self.client_address[0],
                self.client_address[1], str(answer))

        if metodo == "INVITE":
            if nombreusuario in self.registro:
                writes = "SIP/2.0 100 Trying SIP/2.0 180 Ring  " \
                           "SIP/2.0 200 OK\r\n"
                log(False, self.direccioncliente[0],
                    self.cdireccioncliente[1], str(writes))
                self.wfile.write(str.encode(writes))
                with open("registered.json", "r") as f:
                    toc = json.load(f)
                    code = list(toc.keys())
                    values = list(toc.values())
                    puerto1 = code[1].split(":")[2]
                    direccionip1 = values[1][0]

                with socket.socket(socket.AF_INET,
                                   socket.SOCK_DGRAM) as my_socket:
                    my_socket.setsockopt(socket.SOL_SOCKET,
                                         socket.SO_REUSEADDR, 1)

                    my_socket.connect((direccionip1, int(puerto1)))

                    my_socket.send(str.encode(receipt1, 'utf-8'))
                    log(False, self.direccioncliente[0],
                        self.direccionclinte[1], str(receipt1))

            else:
                writes = b"SIP/2.0 404 User Not Found\r\n"
                self.wfile.write(b"SIP/2.0 404 User Not Found\r\n")
                log(False, self.direccioncliente[0],
                    self.direccioncliente[1], str(writes))
                self.wfile.write((writes))

        if metodo == "BYE":
            with open("registered.json", "r") as f:
                toc = json.load(f)
                code = list(toc.keys())
                values = list(toc.values())
                puerto1 = code[1].split(":")[2]
                direccionip1 = values[1][0]

            writesss = b"SIP/2.0 200 OK\r\n"
            self.wfile.write(bytes(writesss))
            log(False, self.direccioncliente[0],
                self.direccioncliente[1], str(writesss))
            with socket.socket(socket.AF_INET,
                               socket.SOCK_DGRAM) as my_socket:
                my_socket.setsockopt(socket.SOL_SOCKET,
                                     socket.SO_REUSEADDR, 1)

                my_socket.connect((direccionip1, int(puerto1)))

                my_socket.send(bytes(receipt1, 'utf-8'))
                log(False, self.direccioncliente[0],
                    self.direccioncliente[1], str(receipt1))

        elif metodo not in ("INVITE", "ACK", "BYE", "REGISTER"):
            writeess = b"SIP/2.0 405 Method Not Allowed\r\n"
            self.wfile.write(writeess)
            log(False, self.direccioncliente[0],
                self.direccioncliente[1], str(writeess))

        if metodo == "ACK":
            with open("registered.json", "r") as f:
                toc = json.load(f)
                code = list(toc.keys())
                values = list(toc.values())
                puerto1 = code[1].split(":")[2]
                direccionip1 = values[1][0]

            with socket.socket(socket.AF_INET,
                               socket.SOCK_DGRAM) as my_socket:
                my_socket.setsockopt(socket.SOL_SOCKET,
                                     socket.SO_REUSEADDR, 1)
                my_socket.connect((direccionip1, int(puerto1)))
                my_socket.send(bytes(receipt1, 'utf-8'))
                log(False, self.direccioncliente[0],
                    self.direccioncliente[1], str(receipt1))
        else:
            writteess = b"SIP/2.0 400 Bad Request\r\n"
            self.wfile.write(writteess)
            log(False, self.direccioncliente[0],
                self.client_address[1], str(writteess))


if __name__ == "_main_":

    try:
        configuracion = sys.argv[1]

    except IndexError:

        sys.exit("Usage: python3 proxy_registrar.py config")

    parser = make_parser()
    cHandler = SmallSMILHandlerp()
    parser.setContentHandler(cHandler)
    parser.parse(open(configuracion))

    xml = cHandler.get_tags()
    servidor = xml[0][1]["ip"]
    puerto = xml[0][1]["puerto"]
    log = xml[1][1]["path"]
    contraseña = xml[1][1]["passwdpath"]
    log = xml[2][1]["path"]

    server = socketserver.UDPServer((servidor,
                                     int(puerto)), ProxyEchoHandler)
    listening = "Servidor"
    log(True, servidor, puerto, str(listening))
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        print("Acabamos el proxy")
