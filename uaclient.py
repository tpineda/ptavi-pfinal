"""Cliente."""
import socket
import sys
import simplertp
import secrets
from xml.sax import make_parser
from xml.sax.handler import ContentHandler
import time
import random

timex = time.time()
timey = time.mktime((time.gmtime()[0], time.gmtime()[1], time.gmtime()[2],
                     0, 0, 0, 0, 0, 0))
tiempox = int(timex - timey)
tiempoy = time.strftime('%Y%m%d', time.gmtime(timex))
tiempoz = str(timey) + str(timex)


class SmallSMILHandlerc(ContentHandler):
    """Clase SmallSMILHandlerc."""

    def __init__(self):
        """Lista de datos."""
        self.lista = []
        self.diccionario = {
            "account": ["username", "password"],
            "uaserver": ["ip", "puerto"],
            "rtpaudio": ["puerto"],
            "regproxy": ["ip", "puerto"],
            "log": ["path"],
            "audio": ["path"]
        }

    def startElement(self, elemento, attrs):
        """Elemento."""
        if elemento in self.diccionario:
            lista = {}
            for j in self.diccionario[elemento]:
                lista[j] = attrs.get(j, "")
            self.lista.append([elemento, lista])

    def get_tags(self):
        """get_tags."""
        return self.lista

    def ficheroslog(type, servidorr, puertor, message):
        """ficheroslog."""
        fichero = open(log, "a")
        text = message.replace("\r\n", " ")
        if type:
            Recieved = "Recieved from: "
        else:
            Recieved = "Sent to: "
        fichero.write(tiempox + " " + Recieved + servidorr + ":"
                      + str(puertor) + ": " + text + "\r\n")
        fichero.close()


if __name__ == "__main__":
    try:
        configuracion = str(sys.argv[1])
        metodo = str(sys.argv[2])
        opcion = str(sys.argv[3])

    except IndexError:
        sys.exit("Usage: python3 client.py config")

    parser = make_parser()
    cHandler = SmallSMILHandlerc()
    parser.setContentHandler(cHandler)
    parser.parse(open(configuracion))

    xml = cHandler.get_tags()
    nombreusuario = xml[0][1]["username"]
    contraseña = xml[0][1]["passwd"]
    servidor = xml[1][1]["ip"]
    puerto = xml[1][1]["puerto"]
    rtpuerto = xml[2][1]["puerto"]

    proxydireccion = xml[3][1]["ip"]
    proxypuerto = xml[3][1]["puerto"]
    log = xml[4][1]["path"]
    audio = xml[5][1]["path"]

    if metodo == "REGISTER":
        sending = tiempox + " Sent to " + proxydireccion \
                  + ":" + proxypuerto + ":"
        linea = metodo + ' sip:' + nombreusuario
        linea += ":" + puerto + ' SIP/2.0\r\n' + "Expires: " + opcion + "\r\n"
        log(False, servidor, puerto, str(linea))

    if metodo == "INVITE":
        sending = "Sent to " + proxydireccion + ":" + proxypuerto + ":"
        datos = "v = 0\r\n"
        datos += "o = " + nombreusuario + " " + servidor + "\r\n"
        datos += "s = misesion\r\n"
        datos += "t = 0\r\n"
        datos += "m = audio " + rtpuerto + " RTP\r\n"
        LINEA = metodo + " sip:" + opcion + " SIP/2.0\r\n"
        LINEA += "Content-Type: application/sdp" + "\r\n"
        LINEA += "Content-Length: " + str(len(datos)) + "\r\n\r\n"
        linea = LINEA + datos
        log(False, servidor, puerto, str(linea))

    if metodo == "BYE":
        linea = (metodo + " sip: " + opcion + " SIP/2.0\r\n")
        log(False, servidor, puerto, str(linea))

    log(True, servidor, puerto, str(linea))
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
        my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        my_socket.connect((proxypuerto, int(proxypuerto)))
        my_socket.send(bytes(linea, 'utf-8'))
        dato = my_socket.recv(1024)
        receipt = dato.decode("utf-8")
        received = tiempox + " Recived from: " + proxydireccion
        received += ":" + str(proxypuerto) + ": " + receipt
        log(True, servidor, puerto, str(receipt))

        metodos = sys.argv[2]

        if metodos == "INVITE":
            try:
                receipt1 = receipt.split()[0] + receipt.split()[1] + \
                           receipt.split()[2]
                receipt2 = receipt.split()[3] + receipt.split()[4]
                receipt2 += receipt.split()[5]
                receipt3 = receipt.split()[6] + receipt.split()[7]
                receipt3 += receipt.split()[8]

            except IndexError:
                pass
            if receipt1 == "SIP/2.0100Trying" and receipt2 == "SIP/2.0180Ring":
                if receipt3 == "SIP/2.0200OK":
                    ship1 = tiempox + " Sent to " + proxydireccion
                    ship1 += ":" + proxypuerto + ":"
                    ship2 = "ACK" + " sip:" + opcion + " SIP/2.0\r\n"
                    ship = ship1 + ship2
                    log(False, servidor, puerto, str(ship))
                    with socket.socket(socket.AF_INET,
                                       socket.SOCK_DGRAM) as my_socket:
                        my_socket.setsockopt(socket.SOL_SOCKET,
                                             socket.SO_REUSEADDR, 1)
                        my_socket.connect((proxydireccion,
                                           int(proxypuerto)))

                        my_socket.send(bytes(ship2, 'utf-8'))

                    aleatorio = random.randint(0, 1000)
                    marker = secrets.randbits(1)
                    cabeceraRTP = simplertp.RtpHeader()
                    numer = aleatorio.randint(0, 15)
                    csrcs = [aleatorio.randint(0, 2 ** 10)
                             for _ in range(numer)]
                    cabeceraRTP.set_header(version=2, marker=marker,
                                           payload_type=14,
                                           ssrc=aleatorio,
                                           pad_flag=0,
                                           cc=len(csrcs))
                    cabeceraRTP.setCSRC(csrcs)
                    RTP_audio = simplertp.RtpPayloadMp3(audio)
                    simplertp.send_rtp_packet(cabeceraRTP,
                                              RTP_audio,
                                              servidor,
                                              int(rtpuerto))

            if metodos == "BYE":
                answer = "SIP/2.0 200 OK\r\n"
                sending = tiempoz + " Sent to " + proxydireccion + ":"
                sending += proxypuerto + ":" + answer
                log(False, servidor, puerto, str(sending))
                with socket.socket(socket.AF_INET, socket.SOCK_DGRAM)\
                        as my_socket:
                    my_socket.setsockopt(socket.SOL_SOCKET,
                                         socket.SO_REUSEADDR, 1)
                    my_socket.connect((proxydireccion, int(proxypuerto)))

                    my_socket.send(bytes(answer, 'utf-8'))
